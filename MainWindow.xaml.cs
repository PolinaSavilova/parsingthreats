﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Net;
using System.IO;
using Path = System.IO.Path;
using System.Windows.Forms;
using MessageBox = System.Windows.MessageBox;
using Excel = Microsoft.Office.Interop.Excel;
using System.ComponentModel;
using System.Windows.Navigation;

namespace ExcelParser
{
    public partial class MainWindow : System.Windows.Window
    {
        int threatsOnPage = 15;
        int currentPage = 0;
        List<Threat> threatsList = new List<Threat>();
        readonly string uri = "http://bdu.fstec.ru/documents/files/thrlist.xlsx";
        string fileName = "threatList.xlsx";
        public MainWindow()
        {
            InitializeComponent();
        }
        private void mainLoad(object sender, RoutedEventArgs events)
        {
            MessageBoxResult result = MessageBoxResult.OK;
            if (!File.Exists(Path.GetFullPath(fileName)))
            {
                result = MessageBoxResult.None;
                while ((result != MessageBoxResult.OK)&&(result != MessageBoxResult.No))
                {
                    MessageBox.Show("Начать загрузку списка угроз безопасности!", "Файл отсутствует", MessageBoxButton.OK, MessageBoxImage.Information);
                    try
                    {
                        WebClient webClient = new WebClient();
                        webClient.DownloadFile(uri, fileName);
                        result = MessageBox.Show("Список угроз безопасности успешно загружен", "Зарузка завершена", MessageBoxButton.OK, MessageBoxImage.Information);
                    }
                    catch (Exception)
                    {
                        result = MessageBox.Show("Ошибка при загрузке.\n Повторить попытку?", "Ошибка", MessageBoxButton.YesNo, MessageBoxImage.Error);
                    }
                }
            }
            if (result == MessageBoxResult.OK)
            {
                createThreatListFrom(fileName,ref threatsList);
            } else
            {
                this.Close();
            }
            showThreatsPage(currentPage);
        }

        private void createThreatListFrom(string fileName, ref List<Threat> threatsList)
        {
            threatsList.Clear();
            Excel.Application excel = new Excel.Application();
            excel.Workbooks.Open(Path.GetFullPath(fileName));
            Excel.Worksheet workSheet = (Excel.Worksheet)excel.Workbooks[1].Worksheets[1];
            int rowCount = workSheet.Cells.SpecialCells(Excel.XlCellType.xlCellTypeLastCell).Row;
            for (int i = 3; i <= rowCount; i++)
            {
                threatsList.Add(new Threat
                {
                    Id = workSheet.Cells[i, 1].Value.ToString(),
                    Name = workSheet.Cells[i, 2].Value.ToString(),
                    Description = workSheet.Cells[i, 3].Value.ToString(),
                    Source = workSheet.Cells[i, 4].Value.ToString(),
                    Object = workSheet.Cells[i, 5].Value.ToString(),
                    ConfidentialityViolation = Convert.ToBoolean(workSheet.Cells[i, 6].Value),
                    IntegrityViolation = Convert.ToBoolean(workSheet.Cells[i, 7].Value),
                    AccessibilityViolation = Convert.ToBoolean(workSheet.Cells[i, 8].Value),
                });
            };
            excel.Quit();

        }

        private void showThreatsPage(int pageNumber)
        {
            List<ShortThreat> shortThreats = new List<ShortThreat>();
            for (int i = pageNumber*threatsOnPage; i < (pageNumber+1)*threatsOnPage; i++)
            {
                if (i < threatsList.Count)
                {
                    shortThreats.Add(new ShortThreat()
                    {
                        Id = "УБИ." + threatsList[i].Id,
                        Name = threatsList[i].Name,
                    });
                }
            };
            var bindingList = new BindingList<ShortThreat>(shortThreats);
            var source = new BindingSource(bindingList, null);
            dataGridView.DataSource = source;
            dataGridView.Columns[0].HeaderText = "ID";
            dataGridView.Columns[1].HeaderText = "Название";
            dataGridView.AutoResizeColumn(0);
            dataGridView.Columns[1].Width = 526;
            for (int i = 0; i < dataGridView.Rows.Count; i++)
            {
                dataGridView.Rows[i].Height = 25;
            }
        }

        private void buttonNextClick(object sender, RoutedEventArgs e)
        {
            if ((currentPage+1)*threatsOnPage < threatsList.Count)
            {
                currentPage++;
                showThreatsPage(currentPage);
            }
        }

        private void buttonPrevClick(object sender, RoutedEventArgs e)
        {
            if (currentPage > 0)
            {
                currentPage--;
                showThreatsPage(currentPage);
            }
        }

        private void saveButtonClick(object sender, RoutedEventArgs e)
        {
            Excel.Application excel = new Excel.Application();
            excel.SheetsInNewWorkbook = 1;
            excel.Workbooks.Add();
            Excel.Worksheet sheet = (Excel.Worksheet)excel.Workbooks[1].Worksheets[1];
            sheet.Name = "УБИ";
            sheet.Cells[1, 1].Value = "Идентификатор УБИ";
            sheet.Cells[1, 2].Value = "Наименование УБИ";
            sheet.Cells[1, 3].Value = "Описание";
            sheet.Cells[1, 4].Value = "Источник угрозы (характеристика и потенциал нарушителя)";
            sheet.Cells[1, 5].Value = "Объект воздействия";
            sheet.Cells[1, 6].Value = "Нарушение конфиденциальности";
            sheet.Cells[1, 7].Value = "Нарушение целостности";
            sheet.Cells[1, 8].Value = "Нарушение доступности";

            for (int i = 0; i < threatsList.Count; i++)
            {
                sheet.Cells[i + 2, 1].Value = threatsList[i].Id;
                sheet.Cells[i + 2, 2].Value = threatsList[i].Name;
                sheet.Cells[i + 2, 3].Value = threatsList[i].Description;
                sheet.Cells[i + 2, 4].Value = threatsList[i].Source;
                sheet.Cells[i + 2, 5].Value = threatsList[i].Object;
                sheet.Cells[i + 2, 6].Value = threatsList[i].ConfidentialityViolation.ToString();
                sheet.Cells[i + 2, 7].Value = threatsList[i].IntegrityViolation.ToString();
                sheet.Cells[i + 2, 8].Value = threatsList[i].AccessibilityViolation.ToString();
            };
            SaveFileDialog save = new SaveFileDialog()
            {
                Filter = "MS Excel dosuments (*.xlsx)|*.xlsx",
                DefaultExt = "*.xlsx",
                FileName = "ThreatList",
                
            };
            if (save.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                sheet.SaveAs(save.FileName);
                MessageBox.Show("Файл успешно сохранен!");
            }
            excel.Quit();
        }

        private void buttonUpdateClick(object sender, RoutedEventArgs e)
        {
            List<Threat> newThreatList = new List<Threat>();
            MessageBoxResult result = MessageBoxResult.None;
            while ((result != MessageBoxResult.OK) && (result != MessageBoxResult.No))
            {
                MessageBox.Show("Начать загрузку списка угроз безопасности!", "Update", MessageBoxButton.OK, MessageBoxImage.Information);
                try
                {
                    WebClient webClient = new WebClient();
                    webClient.DownloadFile(uri, fileName);
                    result = MessageBox.Show("Список угроз безопасности успешно загружен", "Зарузка завершена", MessageBoxButton.OK, MessageBoxImage.Information);
                }
                catch (Exception)
                {
                    result = MessageBox.Show("Ошибка при загрузке.\n Повторить попытку?", "Ошибка", MessageBoxButton.YesNo, MessageBoxImage.Error);
                }
            }
            if(result == MessageBoxResult.OK)
            {
                createThreatListFrom(fileName, ref newThreatList);
            }
            tb.Visibility = Visibility.Visible;
            int countChanges = 0;
            tb.Text += "Список Изменений:\n";
            for (int i = 0; i< newThreatList.Count; i++)
            {
                if (i >= threatsList.Count)
                {
                    countChanges++;
                    tb.Text += $"Id - {newThreatList[i].Id}\nName - {newThreatList[i].Name}\nDescription - {newThreatList[i].Description}\nObject - {newThreatList[i].Object}\nSource - {newThreatList[i].Source}\nIntegrityViolation - {newThreatList[i].IntegrityViolation}\nAccessibilityViolation - {newThreatList[i].AccessibilityViolation}\nConfidentialityViolation - {newThreatList[i].ConfidentialityViolation}\n ";
                }
                else
                {
                    bool change = false;
                    if (threatsList[i].Name != newThreatList[i].Name)
                    {
                        tb.Text += $"Id - {newThreatList[i].Id}\nName was - {threatsList[i].Name} => new Name - {newThreatList[i].Name}\n";
                        change = true;
                    }
                    if (threatsList[i].Description != newThreatList[i].Description)
                    {
                        change = true;
                        tb.Text += $"Id - {newThreatList[i].Id}\nDescription was - {threatsList[i].Description} => new Description - {newThreatList[i].Description}\n";
                    }
                    if (threatsList[i].Object != newThreatList[i].Object)
                    {
                        change = true;
                        tb.Text += $"Id - {newThreatList[i].Id}\nObject was - {threatsList[i].Object} => new Object - {newThreatList[i].Object}\n";
                    }
                    if (threatsList[i].Source != newThreatList[i].Source)
                    {
                        change = true;
                        tb.Text += $"Id - {newThreatList[i].Id}\nSource was - {threatsList[i].Source} => new Source - {newThreatList[i].Source}\n";
                    }
                    if (threatsList[i].IntegrityViolation != newThreatList[i].IntegrityViolation)
                    {
                        change = true;
                        tb.Text += $"Id - {newThreatList[i].Id}\nIntegrityViolation was - {threatsList[i].IntegrityViolation} => new IntegrityViolation - {newThreatList[i].IntegrityViolation}\n";
                    }
                    if (threatsList[i].ConfidentialityViolation != newThreatList[i].ConfidentialityViolation)
                    {
                        change = true;
                        tb.Text += $"Id - {newThreatList[i].Id}\nConfidentialityViolation was - {threatsList[i].ConfidentialityViolation} => new ConfidentialityViolation - {newThreatList[i].ConfidentialityViolation}\n";
                    }
                    if (threatsList[i].AccessibilityViolation != newThreatList[i].AccessibilityViolation)
                    {
                        change = true;
                        tb.Text += $"Id - {newThreatList[i].Id}\nAccessibilityViolation was - {threatsList[i].AccessibilityViolation} => new AccessibilityViolation - {newThreatList[i].AccessibilityViolation}\n";
                    }
                    if (change)
                    {
                        countChanges++;
                    }
                }
            }
            MessageBox.Show($"Количество изменений - {countChanges}");
            WindowsFormHost.Visibility = Visibility.Hidden;
            buttonSavChanges.Visibility = Visibility.Visible;
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            createThreatListFrom(fileName, ref threatsList);
            WindowsFormHost.Visibility = Visibility.Visible;
            buttonSavChanges.Visibility = Visibility.Hidden;
            MessageBox.Show("Изменения сохранены");
            showThreatsPage(currentPage);
        }
        private void dataGridViewCellClick(object sender, EventArgs e)
        {
            int id = (int)dataGridView.SelectedCells[0].RowIndex;
            id = id + currentPage * threatsOnPage;
            MessageBox.Show(threatsList[id].Id + "\n" +
                threatsList[id].Name + "\n" +
                threatsList[id].Description + "\n" +
                "Source - " + threatsList[id].Source + "\n" +
                "Object - " + threatsList[id].Object + "\n" +
                "IntegrityViolation - "+threatsList[id].IntegrityViolation + "\n" +
                "AccessibilityViolation - " + threatsList[id].AccessibilityViolation + "\n" +
                "ConfidentialityViolation - " + threatsList[id].ConfidentialityViolation);

        }
    }

     
}