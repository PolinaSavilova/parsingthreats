﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExcelParser
{
    class Threat
    {
        public string  Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string Source { get; set; }
        public string Object { get; set; }
        public bool ConfidentialityViolation { get; set; }
        public bool IntegrityViolation { get; set; }
        public bool AccessibilityViolation { get; set; }
    }
}
